﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AWS_Cloud_Service.Models;

namespace AWS_Cloud_Service.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        static User[] Users = new User[] { };
        //User prod1 = new User { Username = "TestUser01" };
        //User prod2 = new User { Username = "TestUser02" };
        //UsersController.Users = new User[] { prod1, prod2};

        public UsersController()
        {
            User prod1 = new User { Username = "TestUser01" };
            User prod2 = new User { Username = "TestUser02" };
            UsersController.Users = new User[] { prod1, prod2};            
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return Users;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public User Get(int id)
        {
            return UsersController.Users[id];
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
