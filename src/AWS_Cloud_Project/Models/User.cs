﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWS_Cloud_Service.Models
{
    public class User
    {
        private string mUsername = "";
        private string mEmail = "";
        private string mName = "";
        private string mSurname = "";

        public string Username { get { return mUsername; } set { mUsername = value; } }
        public string Email { get { return mEmail; } set { mEmail = value; } }
        public string Name { get { return mName; } set { mName = value; } }
        public string Surname { get { return mSurname; } set { mSurname = value; } }
    }
}
