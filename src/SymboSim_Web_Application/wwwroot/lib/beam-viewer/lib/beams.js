
var camera, scene, renderer, controls;
var gui, commonParametersSetMenu, beamSizesMenu;
var beamMesh, pointsCloudMesh;
var beamMaterial = new THREE.MeshNormalMaterial();
beamMaterial.side = THREE.DoubleSide;



var beamParts = [];
var currentBeamSubtypes = [];
var previousBeamType;
var plane = new THREE.Plane();
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var offset = new THREE.Vector3();
var intersection = new THREE.Vector3();
var intersectedMesh, selectedMesh;

//All the parameters of all the beams
//
var beam_parameters =
{

    approximation: 1000, // to be set manually
    beamLength: 1000,
    beamType: "C",
    beamsTypes: { "C-beam": "C", "Z-beam": "Z", "T-beam": "T", "I-beam": "I", "+-beam": "+", "Box-beam": "B", "X-beam": "X", "Plate": "P", "Shell-beam": "S" },
    beamSubType: 1,
    beamsSubTypes: { "C": { 1: 1, 2: 2 }, "Z": { 1: 1, 2: 2, 3: 3, 4: 4 }, "T": { 1: 1, 2: 2 }, "I": { 1: 1, 2: 2 }, "+": { 1: 1, 2: 2, 3: 3 }, "B": { 1: 1, 2: 2, 3: 3 }, "X": { 1: 1, 2: 2, 3: 3 }, "P": { 1: 1 }, "S": { 1: 1, 2: 2 } },
    twistAngle: 0,
    horizontalBendRadius: 0,
    verticalBendRadius: 0,
    beamDimensions: {
        "C": {
            1: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50 },
            2: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50 }
        },
        "Z": {
            1: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50 },
            2: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50 },
            3: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50, "qw": 45 },
            4: { "h": 100, "bbf": 50, "tbf": 6, "tw": 6, "ttf": 6, "btf": 50, "qw": 45 }
        },
        "T": {
            1: { "h": 100, "btfl": 25, "btfr": 25, "ttfl": 6, "ttfr": 6 },
            2: { "h": 100, "btf": 50, "tw": 6, "ttf": 6 }
        },
        "I": {
            1: { "h": 100, "bbfl": 25, "bbfr": 25, "ttfl": 6, "ttfr": 6, "btfl": 25, "btfr": 25 },
            2: { "h": 100, "bbf": 25, "tbf": 6, "tw": 6, "ttf": 6, "btf": 25 }
        },
        "+": {
            1: { "h": 100, "b": 100, "db1": 47, "tbw": 6, "dt2": 47, "ttw": 6, "dt1": 47, "tf": 6 },
            2: { "h": 100, "b": 100, "d1": 47, "d2": 47, "d3": 47, "tlf": 6, "trf": 6, "tw": 6 },
            3: { "hbf": 47, "blf": 47, "tblf": 3, "brf": 47, "tbrf": 3, "ttlf": 3, "htf": 47, "ttrf": 3 }
        },
        "B": {
            1: { "h": 100, "bf": 100, "tlw": 6, "trw": 6, "tbf": 6, "ttf": 6 },
            2: { "h": 100, "b": 100, "tlw": 6, "trw": 6, "tbf": 6, "ttf": 6 },
            3: { "h1": 100, "b1": 100, "t": 6 }
        },
        "X": {
            1: { "bf": 100, "d2": 47, "tbw": 6, "d1": 47, "ttw": 6, "bbw": 47, "tf": 6, "btw": 47, "qf": 45, "qtw": 0 },
            2: { "bw": 100, "d2": 47, "tbf": 6, "d1": 47, "ttf": 6, "btf": 47, "tw": 6, "bbf": 47, "qw": -45, "qtf": 0 },
            3: { "hbf": 47, "blf": 47, "tblf": 3, "brf": 47, "tbrf": 3, "ttlf": 3, "htf": 47, "ttrf": 3, "ql": 45, "qt": 0 }
        },
        "P": {
            1: { "h": 100, "b": 25 }
        },
        "S": {
            1: { "ro": 50, "ri": 44, "q": 180 },
            2: { "ro": 50, "ri": 44 }
        }
    },
    dimensionNames: {
        "h": "Total height",
        "bbf": "Breadth of bottom flange",
        "tbf": "Thickness of bottom flange",
        "btf": "Breadth of top flange",
        "ttf": "Thickness of top flange",
        "tw": "Thickness of web",
        "qw": "Inclination of Web",
        "btfl": "Breadth of left side of top flange",
        "btfr": "Breadth of right side of top flange",
        "ttfl": "Thickness of left side of top flange",
        "ttfr": "Thickness of right side of top flange",
        "bbfl": "Breadth of bottom flange (left side)",
        "bbfr": "Breadth of bottom flange (right side)",
        "b": "Total breadth",
        "ttw": "Thickness of top web",
        "tbw": "Thickness of bottom web",
        "tf": "Thickness of flange",
        "dt1": "Distance 1 for placing top web",
        "dt2": "Distance 2 for placing top web",
        "db1": "Distance for placing bottom web",
        "tlf": "Thickness of left flange",
        "trf": "Thickness of right flange",
        "d1": "Distance 1 for flange placement",
        "d2": "Distance 2 for flange placement",
        "d3": "Distance 3 for flange placement",
        "ttrf": "Thickness of top right flange",
        "ttlf": "Thickness of top left flange",
        "ttrf": "Thickness of top right flange",
        "tbrf": "Thickness of bottom right flange",
        "tblf": "Thickness of bottom left flange",
        "htf": "Height of top flanges",
        "hbf": "Height of bottom flanges",
        "blf": "Breadth of left flange",
        "brf": "Breadth of right flange",
        "bf": "Total breadth of flange",
        "tlw": "Thickness of left web",
        "trw": "Thickness of right web",
        "h1": "External height",
        "h2": "Internal height",
        "b1": "External breadth",
        "b2": "Internal breadth",
        "t": "Wall thickness",
        "bbw": "Breadth of bottom web",
        "btw": "Breadth of top web",
        "qf": "Incliration of flange",
        "qtw": "Incliration of web (top)",
        "qbw": "Incliration of web (bottom)",
        "dw1": "Distance 1 for placement of web",
        "dw2": "Distance 2 for placement of web",
        "bw": "Breadth of web",
        "qtf": "Incliration of top flange",
        "qbf": "Incliration of bottom flange",
        "qw": "Incliration of web",
        "ql": "Incliration of left flanges",
        "qb": "Incliration of bottom flanges",
        "qr": "Incliration of right flanges",
        "qt": "Incliration of top flanges",
        "ro": "External radius",
        "ri": "Internal radius",
        "q": "Angle of sweep"
    },
    Approximation: 100,
    x: 0,
    y: 0,
    z: 0
};



function init() {
    var canvas = document.getElementById("display");
    renderer = new THREE.WebGLRenderer({ canvas: canvas });
    renderer.setClearColor(0xffffff);
    //renderer.setSize(canvas.clientWidth, canvas.clientHeight);
    //document.body.appendChild( renderer.domElement );


    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 100000);
    camera.position.set(-500, 500, 500);
    camera.lookAt(scene.position);

    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.set(0.0, 0.0, 0.0);

    scene.add(new THREE.AmbientLight(0x777777));

    var light = new THREE.PointLight(0xffffff);
    light.position.set(5000, 1000, -5000);
    scene.add(light);

    drawBeam();

    renderer.domElement.addEventListener('mousemove', onMouseMove, false);
    renderer.domElement.addEventListener('mousedown', onMouseDown, false);
    renderer.domElement.addEventListener('mouseup', onMouseUp, false);
    renderer.domElement.addEventListener('resize', onWindowResize, false);
}

function animate() {
    onWindowResize()
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
}


window.onload = function () {
    //The control block for the beams parameters
    //  all possible values and subvalues are defined here
    //
    gui = new dat.GUI();
    commonParametersSetMenu = gui.addFolder("Common beam parameters:");
    commonParametersSetMenu.add(beam_parameters, 'beamLength').name("Beam Length").onFinishChange(function () {
        drawBeam();
    });
    commonParametersSetMenu.add(beam_parameters, "twistAngle").name("Twist Angle").onFinishChange(function () {
        drawBeam();
    });
    commonParametersSetMenu.add(beam_parameters, "horizontalBendRadius").name("Horizontal Bend Radius").onFinishChange(function () {
        drawBeam();
    });
    commonParametersSetMenu.add(beam_parameters, "verticalBendRadius").name("Vertical Bend Radius").onFinishChange(function () {
        drawBeam();
    });

    commonParametersSetMenu.add(beam_parameters, "beamType", beam_parameters.beamsTypes).name("Beam Type").onFinishChange(function () {
        commonParametersSetMenu.__controllers[5].remove(); // 5 is index of beam subtype selection drop-down list
        commonParametersSetMenu.add(beam_parameters, "beamSubType", beam_parameters.beamsSubTypes[beam_parameters.beamType]).name("Beam Subtype").onFinishChange(function () {
            addBeamDimensionsMenu();
            drawBeam();
        });
        addBeamDimensionsMenu();
        drawBeam();
    });
    // Beam subtype selection drop-down list is better be the last item because it is deleted and added again when beam type is changed
    //
    commonParametersSetMenu.add(beam_parameters, "beamSubType", beam_parameters.beamsSubTypes[beam_parameters.beamType]).onFinishChange(function () {
        addBeamDimensionsMenu();
        drawBeam();
    });

    beamSizesMenu = gui.addFolder("Beam dimensions:");

    addBeamDimensionsMenu();

    commonParametersSetMenu.open();
    beamSizesMenu.open();


    var xAxisMaterial = new THREE.LineBasicMaterial({ color: 0xff0000 });
    var xAxisGeometry = new THREE.Geometry();
    xAxisGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
    xAxisGeometry.vertices.push(new THREE.Vector3(100000000, 0, 0));
    var xAxisLine = new THREE.Line(xAxisGeometry, xAxisMaterial);
    scene.add(xAxisLine);

    var yAxisMaterial = new THREE.LineBasicMaterial({ color: 0x00ff00 });
    var yAxisGeometry = new THREE.Geometry();
    yAxisGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
    yAxisGeometry.vertices.push(new THREE.Vector3(0, 100000000, 0));
    var yAxisLine = new THREE.Line(yAxisGeometry, yAxisMaterial);
    scene.add(yAxisLine);

    var zAxisMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff });
    var zAxisGeometry = new THREE.Geometry();
    zAxisGeometry.vertices.push(new THREE.Vector3(0, 0, 0));
    zAxisGeometry.vertices.push(new THREE.Vector3(0, 0, 100000000));
    var zAxisLine = new THREE.Line(zAxisGeometry, zAxisMaterial);
    scene.add(zAxisLine);
}

function addBeamDimensionsMenu() {
    while (beamSizesMenu.__controllers.length > 0) { beamSizesMenu.__controllers[beamSizesMenu.__controllers.length - 1].remove() };
    for (var dimensionName in beam_parameters.beamDimensions[beam_parameters.beamType][beam_parameters.beamSubType]) {
        beamSizesMenu.add(beam_parameters.beamDimensions[beam_parameters.beamType][beam_parameters.beamSubType], dimensionName).name("(" + dimensionName + ") " + beam_parameters.dimensionNames[dimensionName]).onFinishChange(function () {

            drawBeam();
        });
    }
}

function drawBeam() {
    //		var possibleSubtypeIndex;
    //		for ( possibleSubtypeIndex = 1; possibleSubtypeIndex <  beam_parameters.beamDimensions[ beam_parameters.beamType ]
    //		if( !beam_parameters.beamSubType )


    if (!beam_parameters.beamsSubTypes[beam_parameters.beamType][beam_parameters.beamSubType]) {
        if (!currentBeamSubtypes[beam_parameters.beamType]) {
            beam_parameters.beamSubType = 1;

        }
        else {
            beam_parameters.beamSubType = currentBeamSubtypes[beam_parameters.beamType];
        }

        commonParametersSetMenu.__controllers[5].updateDisplay();
        addBeamDimensionsMenu();

    }
    else {
        if (previousBeamType != beam_parameters.beamType) {//to be rewritten
            if (currentBeamSubtypes[beam_parameters.beamType]) {
                beam_parameters.beamSubType = currentBeamSubtypes[beam_parameters.beamType];
                commonParametersSetMenu.__controllers[5].updateDisplay();
                addBeamDimensionsMenu();
            }
        }
    }

    previousBeamType = beam_parameters.beamType;
    currentBeamSubtypes[beam_parameters.beamType] = beam_parameters.beamSubType;



    if (beamParts.length > 0) {
        for (var partIndex = 0; partIndex < beamParts.length; partIndex++) {
            scene.remove(beamParts[partIndex]);
        }
    }

    var beamSectionPoints = [];
    var beamPartSectionPoints = [];
    var endface_triangles = [];
    var eft = [];



    var bp = beam_parameters.beamDimensions[beam_parameters.beamType][beam_parameters.beamSubType];

    switch (beam_parameters.beamType) {
        case "C":
            var h = bp.h;
            var bbf = bp.bbf;
            var tbf = bp.tbf;
            var tw = bp.tw;
            var ttf = bp.ttf;
            var btf = bp.btf;


            switch (parseInt(beam_parameters.beamSubType)) {
                case 1:

                    // Defining beam section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, tw);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h - ttf, tw);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, h - ttf, btf);
                    beamPartSectionPoints[6] = new THREE.Vector3(0, h, btf);
                    beamPartSectionPoints[7] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 4 };
                    eft[3] = { v1: 0, v2: 4, v3: 7 };
                    eft[4] = { v1: 4, v2: 5, v3: 6 };
                    eft[5] = { v1: 4, v2: 6, v3: 7 };

                    endface_triangles[0] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (bbf + btf) / 4;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);


                    break;

                case 2:

                    // defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tbf, tw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, tw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, 0);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    //  defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - ttf, btf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, btf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (bbf + btf) / 4;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;
            }
            break;


        case "Z":
            var h = bp.h;
            var btf = bp.btf;
            var ttf = bp.ttf;
            var bbf = bp.bbf;
            var tbf = bp.tbf;
            var tw = bp.tw;

            switch (parseInt(beam_parameters.beamSubType)) {
                case 1:

                    // Defining beam section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, tw);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h, tw);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, h, tw - btf);
                    beamPartSectionPoints[6] = new THREE.Vector3(0, h - ttf, tw - btf);
                    beamPartSectionPoints[7] = new THREE.Vector3(0, h - ttf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 7 };
                    eft[3] = { v1: 3, v2: 4, v3: 7 };
                    eft[4] = { v1: 7, v2: 4, v3: 5 };
                    eft[5] = { v1: 5, v2: 6, v3: 7 };

                    endface_triangles[0] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (bbf + btf - tw) / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 2:

                    // Defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tbf, tw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, tw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, 0);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, tw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h, tw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, tw - btf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, tw - btf);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (bbf + btf - tw) / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 3:
                    var q = bp.qw / 180 * Math.PI;
                    var tanqw = Math.tan(q);

                    var h1 = h - tbf;
                    var h2 = h - ttf;

                    var v1 = new THREE.Vector3(0, 0, Math.tan(Math.PI / 2 - q) * tbf);
                    var v2 = new THREE.Vector3(0, 0, tw / Math.cos(Math.PI / 2 - q));

                    var bbf2 = new THREE.Vector3(0, 0, 0);
                    bbf2.add(tbf).add(v1).add(v2);


                    var inclirationVector1 = new THREE.Vector3(0, h1, h1 / tanqw);
                    var inclirationVector2 = new THREE.Vector3(0, h2, h2 / tanqw);



                    // Defining beam section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);

                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[3].add(v1).add(v2);

                    beamPartSectionPoints[4] = new THREE.Vector3();
                    beamPartSectionPoints[4].copy(beamPartSectionPoints[3]).add(inclirationVector1);

                    beamPartSectionPoints[5] = new THREE.Vector3();
                    beamPartSectionPoints[5].copy(beamPartSectionPoints[4]).add(new THREE.Vector3(0, 0, -btf));

                    beamPartSectionPoints[6] = new THREE.Vector3();
                    beamPartSectionPoints[6].copy(beamPartSectionPoints[5]).add(new THREE.Vector3(0, -ttf, 0));

                    beamPartSectionPoints[7] = new THREE.Vector3();
                    beamPartSectionPoints[7].copy(inclirationVector2);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 7 };
                    eft[3] = { v1: 3, v2: 4, v3: 7 };
                    eft[4] = { v1: 7, v2: 4, v3: 5 };
                    eft[5] = { v1: 5, v2: 6, v3: 7 };

                    endface_triangles[0] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (beamSectionPoints[0][1].z + beamSectionPoints[0][5].z) / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 4:
                    var q = bp.qw / 180 * Math.PI;
                    var tanqw = Math.tan(q);

                    var hh = h - tbf - ttf;
                    var inclirationVector = new THREE.Vector3(0, hh, hh / tanqw);

                    var v1 = new THREE.Vector3(0, 0, Math.tan(Math.PI / 2 - q) * tbf);
                    var v2 = new THREE.Vector3(0, 0, tw / Math.cos(Math.PI / 2 - q));

                    // Defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[3].add(v1);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[0].add(v1);

                    beamPartSectionPoints[1] = new THREE.Vector3();
                    beamPartSectionPoints[1].copy(beamPartSectionPoints[0]).add(v2);

                    beamPartSectionPoints[2] = new THREE.Vector3();
                    beamPartSectionPoints[2].copy(beamPartSectionPoints[1]).add(inclirationVector);

                    beamPartSectionPoints[3] = new THREE.Vector3();
                    beamPartSectionPoints[3].copy(beamPartSectionPoints[2]).sub(v2);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];


                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[0].add(v1).add(v2).add(inclirationVector);

                    beamPartSectionPoints[1] = new THREE.Vector3();
                    beamPartSectionPoints[1].copy(beamPartSectionPoints[0]).add(new THREE.Vector3(0, ttf, ttf / tanqw));

                    beamPartSectionPoints[2] = new THREE.Vector3();
                    beamPartSectionPoints[2].copy(beamPartSectionPoints[1]).add(new THREE.Vector3(0, 0, -btf));

                    beamPartSectionPoints[3] = new THREE.Vector3();
                    beamPartSectionPoints[3].copy(beamPartSectionPoints[2]).add(new THREE.Vector3(0, -ttf, 0));



                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = (beamSectionPoints[0][1].z + beamSectionPoints[2][2].z) / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;
            }
            break;

        case "T":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {
                case 1:

                    var ttfl = bp.ttfl;
                    var ttfr = bp.ttfr;
                    var btfl = bp.btfl;
                    var btfr = bp.btfr;

                    // Defining beam right part section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, ttfr);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttfr, ttfr);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttfr, btfr);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h, btfr);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining beam left part section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h, 0);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, -btfl);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttfl, -btfl);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h - ttfl, -ttfl);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, 0, -ttfl);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 5 };
                    eft[1] = { v1: 1, v2: 2, v3: 5 };
                    eft[2] = { v1: 2, v2: 3, v3: 4 };
                    eft[3] = { v1: 2, v2: 4, v3: 5 };

                    endface_triangles[0] = eft;
                    eft = [];

                    eft[0] = { v1: 5, v2: 0, v3: 1 };
                    eft[1] = { v1: 5, v2: 1, v3: 4 };
                    eft[2] = { v1: 4, v2: 1, v3: 3 };
                    eft[3] = { v1: 3, v2: 1, v3: 2 };

                    endface_triangles[1] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = 0;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);


                    break;

                case 2:
                    var tw = bp.tw;
                    var ttf = bp.ttf;
                    var btf = bp.btf;

                    // Defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, -tw / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, +tw / 2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, +tw / 2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, -tw / 2);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, -btf / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - ttf, btf / 2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, btf / 2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, -btf / 2);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = 0;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;
            }
            break;

        case "I":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {
                case 1:

                    var bbfr = bp.bbfr;
                    var ttfr = bp.ttfr;
                    var btfr = bp.btfr;
                    var bbfl = bp.bbfl;
                    var ttfl = bp.ttfl;
                    var btfl = bp.btfl;

                    // Defining beam right part section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbfr);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, ttfr, bbfr);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, ttfr, ttfr);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h - ttfr, ttfr);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, h - ttfr, btfr);
                    beamPartSectionPoints[6] = new THREE.Vector3(0, h, btfr);
                    beamPartSectionPoints[7] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining beam left part section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h, 0);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, -btfl);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttfl, -btfl);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, h - ttfl, -ttfl);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, ttfl, -ttfl);
                    beamPartSectionPoints[6] = new THREE.Vector3(0, ttfl, -bbfl);
                    beamPartSectionPoints[7] = new THREE.Vector3(0, 0, -bbfl);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //					
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 7 };
                    eft[3] = { v1: 3, v2: 4, v3: 7 };
                    eft[4] = { v1: 4, v2: 5, v3: 6 };
                    eft[5] = { v1: 4, v2: 6, v3: 7 };

                    endface_triangles[0] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 5, v3: 7 };
                    eft[1] = { v1: 7, v2: 5, v3: 6 };
                    eft[2] = { v1: 0, v2: 1, v3: 5 };
                    eft[3] = { v1: 5, v2: 1, v3: 4 };
                    eft[4] = { v1: 4, v2: 1, v3: 3 };
                    eft[5] = { v1: 3, v2: 1, v3: 2 };

                    endface_triangles[1] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = 0;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //					
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);


                    break;

                case 2:
                    var tw = bp.tw;
                    var ttf = bp.ttf;
                    var btf = bp.btf;
                    var tbf = bp.tbf;
                    var bbf = bp.bbf;

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, -bbf / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bbf / 2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bbf / 2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, -bbf / 2);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, -tw / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tbf, +tw / 2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, +tw / 2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, -tw / 2);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, -btf / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - ttf, btf / 2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, bbf / 2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, -bbf / 2);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = 0;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;
            }
            break;

        case "+":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {

                case 1:
                    var b = bp.b;
                    var ttw = bp.ttw;
                    var tbw = bp.tbw;
                    var dt1 = bp.dt1;
                    var dt2 = bp.dt2;
                    var db1 = bp.db1;
                    var tf = bp.tf;

                    // Defining bottom web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, db1);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, db1 + tbw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - dt1 - tf, db1 + tbw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - dt1 - tf, db1);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - dt1 - tf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - dt1 - tf, b);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - dt1, b);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - dt1, 0);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - dt1, dt2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - dt1, dt2 + ttw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, dt2 + ttw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, dt2);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = b / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 2:
                    var b = bp.b;
                    var tw = bp.tw;
                    var tlf = bp.tlf;
                    var trf = bp.trf;
                    var d1 = bp.d1;
                    var d2 = bp.d2;
                    var d3 = bp.d3;

                    // Defining bottom web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, d2, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, d2, d1);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, d2 + tlf, d1);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, d2 + tlf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, d1);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, d1 + tw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, d1 + tw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, d1);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, d3, d1 + tw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, d3, b);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, d3 + trf, b);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, d3 + trf, d1 + tw);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = b / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 3:
                    var blf = bp.blf;
                    var tblf = bp.tblf;
                    var tbrf = bp.tbrf;
                    var brf = bp.brf;
                    var ttrf = bp.ttrf;
                    var ttlf = bp.ttlf;
                    var htf = bp.htf;
                    var hbf = bp.hbf;

                    // Defining bottom left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, blf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, blf + tblf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf, 0);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf, 0);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf, blf);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining bottom right fange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, blf + tblf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, blf + tblf + tbrf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf - tbrf, blf + tblf + tbrf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf - tbrf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf, blf + tblf);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top right flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, hbf + tblf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf + ttrf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf + ttrf, blf + tblf + ttrf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf + ttrf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, hbf + tblf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf - ttlf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf + ttlf, blf + tblf - ttlf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf + ttlf, 0);

                    beamSectionPoints[3] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 5 };
                    eft[2] = { v1: 5, v2: 2, v3: 4 };
                    eft[3] = { v1: 4, v2: 2, v3: 3 };
                    endface_triangles[0] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 5 };
                    eft[2] = { v1: 5, v2: 2, v3: 4 };
                    eft[3] = { v1: 4, v2: 2, v3: 3 };
                    endface_triangles[1] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 5 };
                    eft[3] = { v1: 3, v2: 4, v3: 5 };
                    endface_triangles[2] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 4 };
                    eft[1] = { v1: 0, v2: 4, v3: 5 };
                    eft[2] = { v1: 1, v2: 2, v3: 4 };
                    eft[3] = { v1: 4, v2: 2, v3: 3 };
                    endface_triangles[3] = eft;
                    eft = [];


                    // defining the middle point of beam section
                    //
                    var deltaZ = blf + tblf;
                    var deltaY = hbf + tblf;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;
            }
            break;
        case "B":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {

                case 1:
                    var bf = bp.bf;
                    var ttf = bp.ttf;
                    var tbf = bp.tbf;
                    var tlw = bp.tlw;
                    var trw = bp.trw;

                    // Defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, bf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - ttf, bf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, bf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tbf, tlw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, tlw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, 0);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining right flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tbf, bf - trw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tbf, bf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h - ttf, bf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h - ttf, bf - trw);

                    beamSectionPoints[3] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    endface_triangles[3] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = bf / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 2:
                    var b = bp.b;
                    var ttf = bp.ttf;
                    var tbf = bp.tbf;
                    var tlw = bp.tlw;
                    var trw = bp.trw;

                    // Defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, tlw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, b - trw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tbf, b - trw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tbf, tlw);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, h - ttf, tlw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, h - ttf, b - trw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, b - trw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, tlw);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, tlw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, tlw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining right flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, b - trw);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, b);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, b);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, b - trw);

                    beamSectionPoints[3] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    endface_triangles[1] = eft;
                    endface_triangles[2] = eft;
                    endface_triangles[3] = eft;
                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = b / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 3:
                    var h1 = bp.h1;
                    var b1 = bp.b1;
                    var t = bp.t;

                    // Defining beam section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, b1);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h1, b1);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h1, 0);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, t, 0);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, t, t);
                    beamPartSectionPoints[6] = new THREE.Vector3(0, h1 - t, t);
                    beamPartSectionPoints[7] = new THREE.Vector3(0, h1 - t, b1 - t);
                    beamPartSectionPoints[8] = new THREE.Vector3(0, t, b1 - t);
                    beamPartSectionPoints[9] = new THREE.Vector3(0, t, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];



                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 8 };
                    eft[1] = { v1: 0, v2: 8, v3: 4 };
                    eft[2] = { v1: 8, v2: 1, v3: 2 };
                    eft[3] = { v1: 8, v2: 2, v3: 7 };
                    eft[4] = { v1: 7, v2: 2, v3: 3 };
                    eft[5] = { v1: 7, v2: 3, v3: 6 };
                    eft[6] = { v1: 6, v2: 3, v3: 4 };
                    eft[7] = { v1: 4, v2: 5, v3: 6 };

                    endface_triangles[0] = eft;

                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = b1 / 2;
                    var deltaY = h1 / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

            }
            break;

        case "X":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {

                case 1:
                    var ttw = bp.ttw;
                    var tbw = bp.tbw;
                    var d1 = bp.d1;
                    var d2 = bp.d2;
                    var bbw = bp.bbw;
                    var btw = bp.btw;
                    var bf = bp.bf;
                    var tf = bp.tf;
                    var qf = bp.qf / 180 * Math.PI;
                    var qtw = bp.qtw / 180 * Math.PI;

                    // Defining flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tf, bf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tf, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    eft = [];


                    // Defining top web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tf, bf - d1 - ttw / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tf, bf - d1);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tf + btw, bf - d1);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tf + btw, bf - d1 - ttw);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, tf, bf - d1 - ttw);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 4 };

                    endface_triangles[1] = eft;
                    eft = [];


                    // Defining bottom web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, bf - d2 - tbw / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bf - d2 - tbw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, -bbw, bf - d2 - tbw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, -bbw, bf - d2);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, 0, bf - d2);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 4 };

                    endface_triangles[2] = eft;
                    eft = [];



                    // Defining the middle point of beam section
                    //
                    var deltaZ = bf / 2;
                    var deltaY = tf / 2;
                    var deltaX = beam_parameters.beamLength / 2;


                    // Rotating top web at qtw angle
                    //
                    var qtw_2 = Math.PI / 2 - qtw;
                    var qtw_3 = Math.PI / 2 - qtw_2;

                    var dy = (ttw / 2) * Math.sin(qtw_3);
                    var dz = dy / Math.tan(qtw_2);

                    var cz = beamSectionPoints[1][0].z;
                    var cy = beamSectionPoints[1][0].y;

                    for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[1].length; sectionPointIndex++) {

                        beamSectionPoints[1][sectionPointIndex] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[1][sectionPointIndex].y, beamSectionPoints[1][sectionPointIndex].z, cy, cz, qtw),
getZrotated(beamSectionPoints[1][sectionPointIndex].y, beamSectionPoints[1][sectionPointIndex].z, cy, cz, qtw)
);
                    }


                    // Correcting top web connection with the flange
                    //
                    beamSectionPoints[1][1].z += dz;
                    beamSectionPoints[1][1].y -= dy;
                    beamSectionPoints[1][4].z -= dz;
                    beamSectionPoints[1][4].y += dy;


                    // Rotating bottom web at qtw angle
                    //						
                    var cz = beamSectionPoints[2][0].z;
                    var cy = beamSectionPoints[2][0].y;

                    for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[2].length; sectionPointIndex++) {

                        beamSectionPoints[2][sectionPointIndex] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[2][sectionPointIndex].y, beamSectionPoints[2][sectionPointIndex].z, cy, cz, qtw),
getZrotated(beamSectionPoints[2][sectionPointIndex].y, beamSectionPoints[2][sectionPointIndex].z, cy, cz, qtw)
);
                    }

                    // Correcting bottom web connection with the flange
                    //
                    beamSectionPoints[2][1].z -= dz;
                    beamSectionPoints[2][1].y += dy;
                    beamSectionPoints[2][4].z += dz;
                    beamSectionPoints[2][4].y -= dy;


                    var sectionPointIndex, sectionPartIndex;
                    var pointY, pointZ;

                    for (sectionPartIndex = 0; sectionPartIndex < beamSectionPoints.length; sectionPartIndex++) {
                        for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[sectionPartIndex].length; sectionPointIndex++) {
                            pointY = beamSectionPoints[sectionPartIndex][sectionPointIndex].y;
                            pointZ = beamSectionPoints[sectionPartIndex][sectionPointIndex].z;

                            beamSectionPoints[sectionPartIndex][sectionPointIndex].y = getYrotated(pointY, pointZ, deltaY, deltaZ, qf);
                            beamSectionPoints[sectionPartIndex][sectionPointIndex].z = getZrotated(pointY, pointZ, deltaY, deltaZ, qf);
                            //alert( "pointY: " + pointY + "\n pointZ: " + pointZ + "\n y: " + beamSectionPoints[ sectionPartIndex ][ sectionPointIndex ].y + "\n z: " + beamSectionPoints[ sectionPartIndex ][ sectionPointIndex ].z + "\n sectionPartIndex: " + sectionPartIndex + "\n sectionPointIndex: " + sectionPointIndex );
                        }
                    }



                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

                case 2:

                    var ttf = bp.ttf;
                    var tbf = bp.tbf;
                    var tw = bp.tw;
                    var d1 = bp.d1;
                    var d2 = bp.d2;
                    var bw = bp.bw;
                    var btf = bp.btf;
                    var bbf = bp.bbf;
                    var qw = bp.qw / 180 * Math.PI;
                    var qtf = bp.qtf / 180 * Math.PI;

                    // Defining web section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, bw);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tw, bw);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tw, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;
                    eft = [];


                    // Defining top flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, tw, d1 + ttf / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, tw, d1 + ttf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, tw + btf, d1 + ttf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, tw + btf, d1);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, tw, d1);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 4 };

                    endface_triangles[1] = eft;
                    eft = [];


                    // Defining bottom flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, d2 + tbf / 2);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, d2);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, -bbf, d2);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, -bbf, d2 + tbf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, 0, d2 + tbf);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 4 };

                    endface_triangles[2] = eft;
                    eft = [];



                    // defining the middle point of beam section
                    //
                    var deltaZ = bw / 2;
                    var deltaY = tw / 2;
                    var deltaX = beam_parameters.beamLength / 2;


                    // Rotating top and bottom flanges
                    //

                    //Rotating top flange
                    //
                    var qtf_2 = Math.PI / 2 - qtf;
                    var qtf_3 = Math.PI / 2 - qtf_2;

                    var dy = (ttf / 2) * Math.sin(qtf_3);
                    var dz = dy / Math.tan(qtf_2);

                    var cz = beamSectionPoints[1][0].z;
                    var cy = beamSectionPoints[1][0].y;

                    for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[1].length; sectionPointIndex++) {

                        beamSectionPoints[1][sectionPointIndex] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[1][sectionPointIndex].y, beamSectionPoints[1][sectionPointIndex].z, cy, cz, qtf),
getZrotated(beamSectionPoints[1][sectionPointIndex].y, beamSectionPoints[1][sectionPointIndex].z, cy, cz, qtf)
);
                    }


                    // Correcting top flange connection with the web
                    //
                    beamSectionPoints[1][1].z += dz;
                    beamSectionPoints[1][1].y -= dy;
                    beamSectionPoints[1][4].z -= dz;
                    beamSectionPoints[1][4].y += dy;


                    // Rotating bottom flange 
                    //						
                    var cz = beamSectionPoints[2][0].z;
                    var cy = beamSectionPoints[2][0].y;

                    for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[2].length; sectionPointIndex++) {

                        beamSectionPoints[2][sectionPointIndex] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[2][sectionPointIndex].y, beamSectionPoints[2][sectionPointIndex].z, cy, cz, qtf),
getZrotated(beamSectionPoints[2][sectionPointIndex].y, beamSectionPoints[2][sectionPointIndex].z, cy, cz, qtf)
);
                    }

                    // Correcting bottom flange connection with the web
                    //
                    beamSectionPoints[2][1].z -= dz;
                    beamSectionPoints[2][1].y += dy;
                    beamSectionPoints[2][4].z += dz;
                    beamSectionPoints[2][4].y -= dy;



                    // Rotating all points by qw angle
                    //
                    var sectionPointIndex, sectionPartIndex;
                    var pointY, pointZ;

                    for (sectionPartIndex = 0; sectionPartIndex < beamSectionPoints.length; sectionPartIndex++) {
                        for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[sectionPartIndex].length; sectionPointIndex++) {
                            pointY = beamSectionPoints[sectionPartIndex][sectionPointIndex].y;
                            pointZ = beamSectionPoints[sectionPartIndex][sectionPointIndex].z;

                            beamSectionPoints[sectionPartIndex][sectionPointIndex].y = getYrotated(pointY, pointZ, deltaY, deltaZ, qw);
                            beamSectionPoints[sectionPartIndex][sectionPointIndex].z = getZrotated(pointY, pointZ, deltaY, deltaZ, qw);
                        }
                    }

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);
                    break;

                case 3:
                    var blf = bp.blf;
                    var tblf = bp.tblf;
                    var tbrf = bp.tbrf;
                    var brf = bp.brf;
                    var ttrf = bp.ttrf;
                    var ttlf = bp.ttlf;
                    var htf = bp.htf;
                    var hbf = bp.hbf;

                    var ql = bp.ql / 180 * Math.PI;
                    var qt = bp.qt / 180 * Math.PI;

                    // Defining bottom left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, blf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, blf + tblf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf, 0);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf, 0);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf, blf);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining bottom right fange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, blf + tblf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, blf + tblf + tbrf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf - tbrf, blf + tblf + tbrf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf - tbrf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf, blf + tblf);

                    beamSectionPoints[1] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top right flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, hbf + tblf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf + ttrf, blf + tblf + tbrf + brf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf + ttrf, blf + tblf + ttrf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf + ttrf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf);

                    beamSectionPoints[2] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining top left flange section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, hbf + tblf, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, hbf + tblf, blf + tblf);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, hbf + tblf + ttlf + htf, blf + tblf - ttlf);
                    beamPartSectionPoints[4] = new THREE.Vector3(0, hbf + tblf + ttlf, blf + tblf - ttlf);
                    beamPartSectionPoints[5] = new THREE.Vector3(0, hbf + tblf + ttlf, 0);

                    beamSectionPoints[3] = beamPartSectionPoints;
                    beamPartSectionPoints = [];

                    // Defining endface triangles
                    //
                    /*eft[0] = {v1: 0, v2: 1, v3: 2};
                    eft[1] = {v1: 0, v2: 2, v3: 6};
                    eft[2] = {v1: 6, v2: 2, v3: 3};
                    eft[3] = {v1: 6, v2: 3, v3: 4};
                    eft[4] = {v1: 6, v2: 4, v3: 5};*/
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 5 };
                    eft[2] = { v1: 5, v2: 2, v3: 3 };
                    eft[3] = { v1: 5, v2: 3, v3: 4 };
                    endface_triangles[0] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 5 };
                    eft[2] = { v1: 5, v2: 2, v3: 4 };
                    eft[3] = { v1: 4, v2: 2, v3: 3 };
                    endface_triangles[1] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };
                    eft[2] = { v1: 0, v2: 3, v3: 5 };
                    eft[3] = { v1: 3, v2: 4, v3: 5 };
                    endface_triangles[2] = eft;
                    eft = [];

                    eft[0] = { v1: 0, v2: 1, v3: 4 };
                    eft[1] = { v1: 0, v2: 4, v3: 5 };
                    eft[2] = { v1: 1, v2: 2, v3: 4 };
                    eft[3] = { v1: 4, v2: 2, v3: 3 };
                    endface_triangles[3] = eft;
                    eft = [];


                    // defining the middle point of beam section
                    //
                    var deltaZ = blf + tblf;
                    var deltaY = hbf + tblf;
                    var deltaX = beam_parameters.beamLength / 2;


                    //Rotating flanges
                    //

                    // Left side
                    //
                    beamSectionPoints[0][3] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[0][3].y, beamSectionPoints[0][3].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[0][3].y, beamSectionPoints[0][3].z, deltaY, deltaZ, ql)
);
                    beamSectionPoints[0][4] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[0][4].y, beamSectionPoints[0][4].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[0][4].y, beamSectionPoints[0][4].z, deltaY, deltaZ, ql)
);

                    if (ql > 0)
                        beamSectionPoints[0][5].y = beamSectionPoints[0][5].y - tblf / Math.sin(Math.PI / 2 - ql) + tblf - tblf * Math.tan(ql);

                    beamSectionPoints[3][0] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[3][0].y, beamSectionPoints[3][0].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[3][0].y, beamSectionPoints[3][0].z, deltaY, deltaZ, ql)
);

                    beamSectionPoints[3][5] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[3][5].y, beamSectionPoints[3][5].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[3][5].y, beamSectionPoints[3][5].z, deltaY, deltaZ, ql)
);

                    if (ql < 0)
                        beamSectionPoints[3][4].y = beamSectionPoints[3][4].y + ttlf / Math.sin(Math.PI / 2 - ql) - ttlf - ttlf * Math.tan(ql);

                    // Right side
                    //
                    beamSectionPoints[2][1] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[2][1].y, beamSectionPoints[2][1].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[2][1].y, beamSectionPoints[2][1].z, deltaY, deltaZ, ql)
);
                    beamSectionPoints[2][2] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[2][2].y, beamSectionPoints[2][2].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[2][2].y, beamSectionPoints[2][2].z, deltaY, deltaZ, ql)
);

                    if (ql > 0)
                        beamSectionPoints[2][3].y = beamSectionPoints[2][3].y + ttrf / Math.sin(Math.PI / 2 - ql) - ttrf + ttrf * Math.tan(ql);

                    beamSectionPoints[1][3] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[1][3].y, beamSectionPoints[1][3].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[1][3].y, beamSectionPoints[1][3].z, deltaY, deltaZ, ql)
);

                    beamSectionPoints[1][4] = new THREE.Vector3
(
0,
getYrotated(beamSectionPoints[1][4].y, beamSectionPoints[1][4].z, deltaY, deltaZ, ql),
getZrotated(beamSectionPoints[1][4].y, beamSectionPoints[1][4].z, deltaY, deltaZ, ql)
);

                    if (ql < 0)
                        beamSectionPoints[1][2].y = beamSectionPoints[1][2].y - ttlf / Math.sin(Math.PI / 2 - ql) + ttlf + ttlf * Math.tan(ql);


                    // Rotating all the points by qt angle
                    //
                    var sectionPointIndex, sectionPartIndex;
                    var pointY, pointZ;

                    for (sectionPartIndex = 0; sectionPartIndex < beamSectionPoints.length; sectionPartIndex++) {
                        for (sectionPointIndex = 0; sectionPointIndex < beamSectionPoints[sectionPartIndex].length; sectionPointIndex++) {
                            pointY = beamSectionPoints[sectionPartIndex][sectionPointIndex].y;
                            pointZ = beamSectionPoints[sectionPartIndex][sectionPointIndex].z;

                            beamSectionPoints[sectionPartIndex][sectionPointIndex].y = getYrotated(pointY, pointZ, deltaY, deltaZ, qt);
                            beamSectionPoints[sectionPartIndex][sectionPointIndex].z = getZrotated(pointY, pointZ, deltaY, deltaZ, qt);
                        }
                    }

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);



                    break;
            }
            break;

        case "P":
            var h = bp.h;

            switch (parseInt(beam_parameters.beamSubType)) {
                case 1:
                    var b = bp.b;

                    // Defining beam section points
                    //
                    beamPartSectionPoints[0] = new THREE.Vector3(0, 0, 0);
                    beamPartSectionPoints[1] = new THREE.Vector3(0, 0, b);
                    beamPartSectionPoints[2] = new THREE.Vector3(0, h, b);
                    beamPartSectionPoints[3] = new THREE.Vector3(0, h, 0);

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];



                    // Defining endface triangles
                    //
                    eft[0] = { v1: 0, v2: 1, v3: 2 };
                    eft[1] = { v1: 0, v2: 2, v3: 3 };

                    endface_triangles[0] = eft;

                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = b / 2;
                    var deltaY = h / 2;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

            }
            break;

        case "S":
            var h = bp.h;
            var q = bp.q;

            switch (parseInt(beam_parameters.beamSubType)) {

                case 2:
                    q = 360;
                case 1:
                    var b = bp.b;
                    var ro = bp.ro;
                    var ri = bp.ri;
                    var q = q / 180 * Math.PI;
                    var segmentIndex = 0;
                    var segmentsCount = 20;


                    // Defining beam section points
                    //
                    beamPartSectionPoints.push(new THREE.Vector3(0, 0, ro));

                    for (var segmentIndex = 1; segmentIndex <= segmentsCount; segmentIndex++) {
                        beamPartSectionPoints.push(new THREE.Vector3(0, ro * Math.sin(q * (segmentIndex / segmentsCount)), ro * Math.cos(q * (segmentIndex / segmentsCount))));
                    }
                    var pointsCount = segmentsCount;
                    for (var segmentIndex = segmentsCount; segmentIndex >= 1; segmentIndex--) {
                        beamPartSectionPoints.push(new THREE.Vector3(0, ri * Math.sin(q * (segmentIndex / segmentsCount)), ri * Math.cos(q * (segmentIndex / segmentsCount))));
                        pointsCount++;

                        if (segmentIndex > 1) {
                            eft.push({ v1: pointsCount, v2: pointsCount - (segmentsCount - segmentIndex) * 2 - 2, v3: pointsCount - (segmentsCount - segmentIndex) * 2 - 1 });
                            eft.push({ v1: pointsCount, v2: pointsCount + 1, v3: pointsCount - (segmentsCount - segmentIndex) * 2 - 2 });
                        }
                    }

                    beamPartSectionPoints.push(new THREE.Vector3(0, 0, ri));
                    pointsCount++;

                    eft.push({ v1: pointsCount, v2: 0, v3: pointsCount - 1 });
                    eft.push({ v1: 0, v2: 1, v3: pointsCount - 1 });

                    beamSectionPoints[0] = beamPartSectionPoints;
                    beamPartSectionPoints = [];



                    // Defining endface triangles
                    //
                    //eft[0] = {v1: 0, v2: 1, v3: 2};
                    //eft[1] = {v1: 0, v2: 2, v3: 3};

                    endface_triangles[0] = eft;

                    eft = [];

                    // defining the middle point of beam section
                    //
                    var deltaZ = 0;
                    var deltaY = 0;
                    var deltaX = beam_parameters.beamLength / 2;

                    // Building the beam mesh
                    //
                    buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ);

                    break;

            }
            break;

    }

}

function buildBeamMeshes(beamSectionPoints, endface_triangles, deltaX, deltaY, deltaZ) {
    var beamGeometry = new THREE.Geometry();
    var sectionPointsCount;
    var beamPartsIndex, i, j, k, l;
    var beamPartSectionPoints = [];
    beamParts = [];

    for (beamPartsIndex = 0; beamPartsIndex < beamSectionPoints.length; beamPartsIndex++) {
        beamPartSectionPoints = beamSectionPoints[beamPartsIndex];
        sectionPointsCount = beamPartSectionPoints.length;
        beamGeometry = new THREE.Geometry();

        for (i = 0; i < sectionPointsCount; i++) {
            beamGeometry.vertices.push(beamPartSectionPoints[i]);
        }

        for (j = 1; j <= beam_parameters.approximation; j++) {
            var ta = beam_parameters.twistAngle / 180 * Math.PI / beam_parameters.approximation * j;
            var X = beam_parameters.beamLength * (j / beam_parameters.approximation);
            for (var k = 0; k < sectionPointsCount; k++) {
                var y = beamPartSectionPoints[k].y;
                var z = beamPartSectionPoints[k].z;
                beamGeometry.vertices.push(new THREE.Vector3(
X, getYrotated(y, z, deltaY, deltaZ, ta), getZrotated(y, z, deltaY, deltaZ, ta)));
            }

            for (i = 1; i < sectionPointsCount; i++) {
                var p1 = j * sectionPointsCount + i;
                var p2 = j * sectionPointsCount + i - 1;
                var p3 = (j - 1) * sectionPointsCount + i;
                var p4 = (j - 1) * sectionPointsCount + i - 1;

                beamGeometry.faces.push(new THREE.Face3(p2, p1, p4));
                beamGeometry.faces.push(new THREE.Face3(p1, p3, p4));
            }

            var p1 = (j - 1) * sectionPointsCount;
            var p2 = (j - 1) * sectionPointsCount + sectionPointsCount - 1;

            var p3 = j * sectionPointsCount;
            var p4 = j * sectionPointsCount + sectionPointsCount - 1;


            beamGeometry.faces.push(new THREE.Face3(p1, p2, p4));
            beamGeometry.faces.push(new THREE.Face3(p1, p4, p3));
        }

        // Building endfaces geometries
        //
        for (l = 0; l < endface_triangles[beamPartsIndex].length; l++) {
            beamGeometry.faces.push(new THREE.Face3(endface_triangles[beamPartsIndex][l].v1, endface_triangles[beamPartsIndex][l].v2, endface_triangles[beamPartsIndex][l].v3));
        }

        for (l = 0; l < endface_triangles[beamPartsIndex].length; l++) {
            beamGeometry.faces.push(new THREE.Face3(
(endface_triangles[beamPartsIndex][l].v1 + sectionPointsCount * beam_parameters.approximation),
(endface_triangles[beamPartsIndex][l].v3 + sectionPointsCount * beam_parameters.approximation),
(endface_triangles[beamPartsIndex][l].v2 + sectionPointsCount * beam_parameters.approximation)));
        }

        beamGeometry.computeFaceNormals();
        beamMesh = new THREE.Mesh(beamGeometry, beamMaterial);
        beamMesh.position.x = beam_parameters.x;
        beamMesh.position.y = beam_parameters.y;
        beamMesh.position.z = beam_parameters.z;
        beamMesh.name = "beam_part";

        beamParts[beamPartsIndex] = beamMesh;

        scene.add(beamMesh);
    }

}


function getYrotated(Y, Z, centerY, centerZ, angle) {
    var newY = (Y - centerY) * Math.cos(angle) + (Z - centerZ) * Math.sin(angle) + centerY;
    return (newY);
}

function getZrotated(Y, Z, centerY, centerZ, angle) {
    var newZ = (Z - centerZ) * Math.cos(angle) - (Y - centerY) * Math.sin(angle) + centerZ;
    return (newZ);
}

function setCameraPosition(viewType) {
    var lookVector = new THREE.Vector3();
    var cameraPositionVector = new THREE.Vector3();
    var centerPoint = new THREE.Vector3();

    centerPoint.copy(beamParts[0].position);
    centerPoint.x += beam_parameters.beamLength / 2;

    switch (viewType) {
        case "fit_view":

            cameraPositionVector.copy(camera.position);
            lookVector = cameraPositionVector.sub(centerPoint);
            lookVector.normalize();
            lookVector.multiplyScalar(beamMesh.geometry.boundingSphere.radius * 1.8);
            camera.position.x = centerPoint.x + lookVector.x;
            camera.position.y = centerPoint.y + lookVector.y;
            camera.position.z = centerPoint.z + lookVector.z;

            break;

        case "xy_view":

            camera.position.x = centerPoint.x;
            camera.position.y = centerPoint.y;
            camera.position.z = centerPoint.z + beamMesh.geometry.boundingSphere.radius * 1.8;

            break;

        case "yz_view":

            camera.position.x = centerPoint.x + beamMesh.geometry.boundingSphere.radius * 1.8;
            camera.position.y = centerPoint.y;
            camera.position.z = centerPoint.z;

            break;

        case "xz_view":

            camera.position.x = centerPoint.x;
            camera.position.y = centerPoint.y + beamMesh.geometry.boundingSphere.radius * 1.8;
            camera.position.z = centerPoint.z;

            break;

        default:

            camera.position.x = centerPoint.x - 500;
            camera.position.y = centerPoint.y + 500;
            camera.position.z = centerPoint.z + 500;
    }

    camera.lookAt(centerPoint);
    controls.target = centerPoint;
}



function onMouseMove(event) {
    var m1 = new THREE.Vector3();
    var m2 = new THREE.Vector3();
    var o = new THREE.Vector3();
    var i = new THREE.Vector3();

    event.preventDefault();

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    raycaster.setFromCamera(mouse, camera);

    if (selectedMesh) {

        if (raycaster.ray.intersectPlane(plane, intersection)) {
            for (var beamPartIndex = 0; beamPartIndex < beamParts.length; beamPartIndex++) {
                if (beamParts[beamPartIndex] != selectedMesh) {
                    m1.copy(selectedMesh.position);
                    m2.copy(beamParts[beamPartIndex].position);
                    o.copy(offset);
                    i.copy(intersection);
                    beamParts[beamPartIndex].position.copy(i.sub(o.add(m1.sub(m2))));

                }
            }
            selectedMesh.position.copy(intersection).sub(offset);
        }

        return;

    }

    var intersects = raycaster.intersectObjects(beamParts);

    if (intersects.length > 0) {

        if (intersectedMesh != intersects[0].object) {
            intersectedMesh = intersects[0].object;

            plane.setFromNormalAndCoplanarPoint(camera.getWorldDirection(plane.normal), intersectedMesh.position);

            for (var beamPartIndex = 0; beamPartIndex < beamParts.length; beamPartIndex++) {
                beamParts[beamPartIndex].material = beamMaterial;
                beamParts[beamPartIndex].needsUpdate = true;
            }

            intersectedMesh.material = new THREE.MeshBasicMaterial({ color: 0xff8888, opacity: 0.5 }); intersectedMesh.material.needsUpdate = true;
        }
    }
    else {
        intersectedMesh = null;
        for (var beamPartIndex = 0; beamPartIndex < beamParts.length; beamPartIndex++) {
            beamParts[beamPartIndex].material = beamMaterial;
            beamParts[beamPartIndex].needsUpdate = true;
        }
    }

}

function onMouseDown(event) {
    event.preventDefault();

    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(beamParts);

    if (intersects.length > 0) {
        controls.enabled = false;

        selectedMesh = intersects[0].object;

        if (raycaster.ray.intersectPlane(plane, intersection)) {
            offset.copy(intersection).sub(selectedMesh.position);

        }

    }

}

function onMouseUp(event) {

    event.preventDefault();

    controls.enabled = true;

    if (intersectedMesh) {
        beam_parameters.x = selectedMesh.position.x;
        beam_parameters.y = selectedMesh.position.y;
        beam_parameters.z = selectedMesh.position.z;
        selectedMesh = null;
    }
    for (var beamPartIndex = 0; beamPartIndex < beamParts.length; beamPartIndex++) {
        beamParts[beamPartIndex].material = beamMaterial;
        beamParts[beamPartIndex].material.needsUpdate = true;
    }

}

function onWindowResize() {
    //windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    var width = renderer.domElement.parentElement.clientWidth;
    var height = renderer.domElement.parentElement.clientHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);

    //renderer.render(scene, camera);
}

function drawPointCloud() {
    if (pointsCloudMesh) scene.remove(pointsCloudMesh);
    var textareaElement = document.getElementById("point_cloud");
    var coordinatesString = textareaElement.value;
    var i, j;
    var x, y, z;
    var coordinatesRecord = [];
    var coordinatesArray = coordinatesString.split(/\n/);

    var geometry = new THREE.Geometry();
    var pointsCount = coordinatesArray.length;
    var min_x = max_x = min_y = max_y = min_z = max_z = delta_x = delta_y = delta_z = 0;
    var r, g, b;

    for (i = 0; i < pointsCount; i++) {
        coordinatesRecord = coordinatesArray[i].split(/\s*,\s/);
        x = parseFloat(coordinatesRecord[1]) * 100;
        y = parseFloat(coordinatesRecord[2]) * 100;
        z = parseFloat(coordinatesRecord[3]) * 100;

        if (x < min_x) min_x = x;
        if (y < min_y) min_y = y;
        if (z < min_z) min_z = z;
        if (x > max_x) max_x = x;
        if (y > max_y) max_y = y;
        if (z > max_z) max_z = z;

        geometry.vertices.push(new THREE.Vector3(x, y, z));
    }

    delta_x = max_x - min_x;
    delta_y = max_y - min_y;
    delta_z = max_z - min_z;

    for (i = 0; i < pointsCount; i++) {
        x = geometry.vertices[i].x;
        y = geometry.vertices[i].y;
        z = geometry.vertices[i].z;

        r = (x - min_x) / delta_x;
        g = (y - min_y) / delta_y;
        b = (z - min_z) / delta_z;

        geometry.colors.push(new THREE.Color(r, g, b));
    }

    var pointsMaterial = new THREE.PointsMaterial({ size: 10, vertexColors: THREE.VertexColors });
    pointsCloudMesh = new THREE.Points(geometry, pointsMaterial);
    scene.add(pointsCloudMesh);
}



